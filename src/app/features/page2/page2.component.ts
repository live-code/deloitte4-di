import { Component } from '@angular/core';
import { LogService } from '../../core/log.service';
import { PowerlogService } from '../../core/powerlog.service';

@Component({
  selector: 'app-page2',
  template: `
    <h1>Page 2</h1>

    <button (click)="logService.show('new msg')">Add log</button>

    <pre>{{logService.logs | json}}</pre>
  `,
  providers: [
    { provide: LogService, useExisting: PowerlogService}
  ]
})
export class Page2Component {
  constructor(public logService: LogService) {
  }
}
