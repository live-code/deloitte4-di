import { Component } from '@angular/core';
import { LogService } from '../../core/log.service';

@Component({
  selector: 'app-page1',
  template: `
    <h1>Page 1</h1>
    
    <button (click)="logService.show('new msg')">Add log</button>
    
    <pre>{{logService.logs | json}}</pre>
  `,
})
export class Page1Component {
  constructor(public logService: LogService) {
  }
}
