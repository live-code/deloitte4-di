import { Component } from '@angular/core';
import { CrudService } from '../../core/crud.service';

@Component({
  selector: 'app-page3',
  template: `
    <p>
      page3 works!
    </p>
    
    <app-widget></app-widget>
    <app-widget></app-widget>
    <app-widget></app-widget>
    <app-panel />
    
    
  `,
  providers: [
    // { provide: WidgetService, useClass: OpenStreetMapService}
  ],
  styles: [
  ]
})
export class Page3Component {
  constructor(private CRUDService: CrudService) {
  }
}
