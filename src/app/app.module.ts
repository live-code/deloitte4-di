import { HttpClient, HttpClientModule } from '@angular/common/http';
import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CrudDevService } from './core/crud-dev.service';
import { CrudService } from './core/crud.service';
import { LogService } from './core/log.service';
import { PowerlogService } from './core/powerlog.service';

export const API_KEY2 = new InjectionToken<string>('apikey2_indifferente')

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    // LogService,
    {
      provide: CrudService,
      useFactory: (http: HttpClient, api: string) => {
        return environment.production ?
          new CrudService(http, 'api1') :
          new CrudDevService(http, api)
      },
      deps: [HttpClient, API_KEY2]
    },
    PowerlogService,
    { provide: 'API_KEY', useValue: 'abc123' },
    { provide: API_KEY2, useValue: 'abcFromToken' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}
