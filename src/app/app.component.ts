import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    
    <button routerLink="page1">page 1</button>
    <button routerLink="page2">page 2</button>
    <button routerLink="page3">page 3</button>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
}
