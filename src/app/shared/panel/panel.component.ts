
import { Component, inject, Injector, Optional, Self } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyService } from '../../core/my.service';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      panel works!
    </p>
    {{title}}
    
    <button (click)="myMethod()">click</button>
  `,
  providers: [
    MyService
  ]
})
export class PanelComponent {
  title = ''
  // myService = inject(MyService);

  constructor(private inj: Injector) {
  }

  myMethod() {
    const myService = this.inj.get(MyService)
    console.log(myService.title)
  }

  // Self: search provide in this component only
  /*constructor(@Self() private myService: MyService) {

  }*/

  // optional service
  /*constructor(@Optional() private myService: MyService) {
    if (myService) {
      this.title = myService.title
    } else {
      this.title = 'hello default panel'
    }
  }*/
}
