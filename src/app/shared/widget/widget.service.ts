import { Injectable } from '@angular/core';

@Injectable()
export class WidgetService {
  isOpened = false;

  toggle() {
    this.isOpened = !this.isOpened
  }
}
