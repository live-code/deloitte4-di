import { Component, Inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { API_KEY2 } from '../../app.module';
import { WidgetService } from './widget.service';

@Component({
  selector: 'app-widget',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      widget works! {{srv.isOpened}}
    </p>
    
    <button (click)="srv.toggle()">toggle</button>
  `,
  providers: [
    WidgetService
  ]
})
export class WidgetComponent {

  constructor(
    public srv: WidgetService,
    @Inject('API_KEY') key: string,
    @Inject(API_KEY2) key2: string,
  ) {
    console.log(key)
    console.log(key2)
  }
}
