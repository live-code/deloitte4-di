import { Injectable } from '@angular/core';

@Injectable()
export class PowerlogService {
  private _logs: string[] = [];

  constructor() {
    console.log('>> power log service constructor')
  }

  show(msg: string) {
    console.log('>> power log', msg)
    this._logs.push('powerlog' + msg)
  }

  get logs(): string[] {
    return this._logs;
  }
}
