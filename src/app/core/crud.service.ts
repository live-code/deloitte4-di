import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { API_KEY2 } from '../app.module';

@Injectable({ providedIn: 'root'})
export class CrudService {

  constructor(private http: HttpClient, @Inject(API_KEY2) apikey: string) {
    console.log('CRUD PROD service constructor', apikey)
  }

}
