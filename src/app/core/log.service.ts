import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  private _logs: string[] = [];

  constructor() {
    console.log('log service constructor')
  }

  show(msg: string) {
    console.log('log', msg)
    this._logs.push(msg)
  }

  get logs(): string[] {
    return this._logs;
  }
}
